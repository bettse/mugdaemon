require('dotenv').config({silent: true})
const debug = require('debug')('mugdaemon')
const tokens = require('twitter-tokens')
const Twitter = require('twitter')
const tinycolor = require('tinycolor2')
const mqtt = require('mqtt');

const ALL = 0
const INCOLOR = tinycolor('fb1453').toRgb()
const INSTANT = 0 // ms
const FAST = 100 // ms
const SLOW = 500 // ms
const HALFSEC = 500 // ms
const topic = "/ble/write/LightMug/nus/nus_tx"
const twitter = new Twitter(tokens)
const mqtt_config = {
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD
}
const client = mqtt.connect(process.env.MQTT_URL, mqtt_config)

var connected = false

client.on('connect', function () {
  debug('Connected')
  connected = true
  twitter.stream('statuses/filter', {track: 'invisionapp'}, handleStream)
  setColor(INSTANT, 0x00, 0x00, 0xFF, ALL, off)
})

function handleStream (stream) {
  debug('Streaming data')
  stream.on('data', newEvent)
  stream.on('error', errorEvent)
  if (daytime() && connected) {
-    setColor(INSTANT, 0x00, 0xFF, 0x00, ALL, off)
  }
}

function newEvent (event) {
  debug('Event', event && event.text)
  setColor(FAST, INCOLOR.r, INCOLOR.g, INCOLOR.b, ALL, function() {
    setTimeout(off, HALFSEC)
  })
}

function errorEvent (error) {
  setColor(INSTANT, 0xFF, 0x00, 0x00, ALL)
  console.error('Twitter error', error)
}

function off () {
  setColor(SLOW, 0x00, 0x00, 0x00, ALL)
}

function daytime() {
  var date = new Date()
  var current_hour = date.getHours()
  // 7am-10pm
  return (current_hour > 6 && current_hour < 22)
}

function setColor(speed, r, g, b, lights, callback) {
  const message = Buffer.from([1, 'c', r, g, b, speed / 0xFF, speed % 0xFF, lights])
  debug("=>", JSON.stringify(message))
  client.publish(topic, JSON.stringify(message))
  if (callback) { callback() }
}

